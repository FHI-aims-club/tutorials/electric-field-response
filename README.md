# Electric field response using DFPT in FHI-aims

This is a tutorial for calculating the response of molecules and crystals to an external homogenous electric field using density functional perturbation theory (DFPT) as implemented in FHI-aims.
Specifically, we are going to be calculating the IR and Raman spectra of i) a benzene molecule and ii) a benzene crystal.

The tutorial is available here:
https://fhi-aims-club.gitlab.io/tutorials/electric-field-response

Input files are available within this repo, in each specific Tutorial directory.