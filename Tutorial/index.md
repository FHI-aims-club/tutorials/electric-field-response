# Electric field response in FHI-aims using DFPT

**Prepared by Connor L. Box, James A. Green, Reinhard J. Maurer and Mariana Rossi**

This is a tutorial for calculating the response of molecules and crystals to an external homogenous electric field using density functional perturbation theory (DFPT) as implemented in FHI-aims.
Specifically, we are going to be calculating the IR and Raman spectra of i) a benzene molecule and ii) a benzene crystal.

Part i) of the tutorial was used in the FHI-aims webinar [Advanced Electronic Structure Methods for Battery Design and Vibrational Spectroscopy 30 January 2025](https://indico.ms1p.org/event/9/).

Input files and solutions are available within this repo, in each specific Tutorial directory.

## Summary of the tutorial

The tutorial is organised as follows:

[**Part 1**](./Tutorial-1/README.md) introduces density functional perturbation theory and response to electric fields.   
[**Part 2**](./Tutorial-2/README.md) simulates the IR and Raman spectra of benzene using DFPT  
[**Part 3**](./Tutorial-3/README.md) simulates the IR and Raman spectra of a benzene crystal using DFPT  
