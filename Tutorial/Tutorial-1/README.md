# Density Functional Perturbation Theory

We refer the interested reader to two implementation papers of density functional perturbation theory (DFPT) in FHI-aims:

[1] [Shang, H., Carbogno, C., Rinke, P., & Scheffler, M. (2017). Lattice dynamics calculations based on density-functional perturbation theory in real space. Computer Physics Communications, 215, 26-46.](https://doi.org/10.1016/j.cpc.2017.02.001)  
[2] [Shang, H., Raimbault, N., Rinke, P., Scheffler, M., Rossi, M., & Carbogno, C. (2018). All-electron, real-space perturbation theory for homogeneous electric fields: theory, implementation, and application within DFT. New Journal of Physics, 20(7), 073040.](https://doi.org/10.1088/1367-2630/aace6d)

## Theory overview

Response functions, which describe how a system reacts to perturbations, underpin numerous crucial properties of a system and directly relate to experimental observations.

When we examine the properties of a system in terms of the derivatives of its total energy, we can categorize them into different orders:

**First-order properties**: These include forces, stress, and dipole moment.  
**Second-order properties**: Examples are the phonon dynamical matrix, elastic constants, dielectric susceptibility, and Born effective charges.  
**Third-order properties**: These encompass non-linear dielectric susceptibility, phonon-phonon interactions, and so on.  

Accessing properties at the $2n+1$ order necessitates knowledge of the perturbed eigenstates at the $n$ order, a fundamental principle known as Wigner's $2n+1$ theorem. For instance, second and third-order properties can be determined with first-order eigenstates.

Methods like DFPT and finite difference approaches enable the computation of perturbed eigenstates, thereby providing access to crucial properties such as the dielectric susceptibility and dynamical matrices.

In materials modeling, calculating vibrational properties from electronic structure data holds significant importance. These properties play pivotal roles in various physical phenomena such as specific heats, thermal expansion, and electron-phonon interactions, thereby influencing phenomena like metal resistivity and superconductivity. Techniques like vibrational spectroscopy, including infrared and Raman spectroscopy, alongside inelastic neutron scattering, offer insights into local bonding and chemical structure. Accurate calculations of vibrational frequencies provide valuable information about atomic and electronic structures.

## Finite difference vs DFPT

Finite difference approaches are based on approximating derivatives numerically by evaluating the system at slightly perturbed versions (whether it be under a small electric field or with atoms slightly displaced) and calculating finite differences between them.
Thus finite difference benefits from the simple implementation as there is no need for an additional theoretical framework beyond electronic structure calculations. It can also handle arbitrary potentials and systems.

In contrast, density functional perturbation theory approaches require extensive implementations beyond the standard electronic structure calculation and some specific tailoring to different systems/response types is required. Thus, it is less trivial to implement than finite difference. The benefits of DFPT are that the expressions are analytical, thus the numerical effects present in finite difference are absent. 

In this tutorial, we will employ finite difference for the response to atomic perturbations, and DFPT for the response to electric fields. In principle, either method can be used to calculate both response types.
