import matplotlib.pyplot as plt
import numpy as np

fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(8, 6), sharex=True)

calc_data = np.loadtxt("displacements/benz_IR_spectrum.csv")
exp_data = np.loadtxt("experimental/exp_benzene_IR.txt",skiprows=34)

ax1.plot(calc_data[:,0],calc_data[:,1],'r',lw=2, label="Simulation")
ax2.plot(exp_data[:,0],(1-exp_data[:,1]),'k',lw=2, label="Experiment")

ax1.set_xlim(500,3400)
ax2.set_xlim(500,3400)
ax1.set_yticks([])
ax2.set_yticks([])
ax2.set_xlabel('Frequency [1/cm]',size=12)
ax2.set_ylabel('Intensity [a.u.]',size=12)
ax2.yaxis.set_label_coords(-0.05, 1)
# Remove space between subplots
plt.subplots_adjust(hspace=0)

fig.legend(loc='upper center',bbox_to_anchor=(0.5,0.85))

plt.tight_layout()
plt.savefig("comp_benzene.png")
plt.show()
 
