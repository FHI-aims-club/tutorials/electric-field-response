#!/bin/bash
#SBATCH --job-name=Raman
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --mem-per-cpu=3700
#SBATCH --time=24:00:00
#SBATCH--mail-type=END
##SBATCH --mail-user=john.doe@warwick.ac.uk

module purge
module load intel/2020b  CMake/3.18.4
# Set the number of threads to 1. This prevents any threaded system libraries from automatically using threading.
export OMP_NUM_THREADS=1

# Set stacksize to unlimited for FHI-aims
ulimit -s unlimited

OUT_FILE=aims.out

AIMS_BIN=/home/chem/msrvhs/software/aims/FHIaims/build_debug/aims.240314.scalapack.mpi.x
srun $AIMS_BIN > $OUT_FILE
#python vib.py > normal_modes
